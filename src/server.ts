import express from 'express';
import bodyParser from 'body-parser';
import { hello } from './routes/hello';
import { register } from './routes/register';
import { login } from './routes/login';
import { user } from './routes/user';
import { Auth } from './shared/auth';

export const createServer = () => {
    // Logic
    const app = express();
    // config
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(Auth.initialize());

    // router
    app.use('/hi', hello);
    app.use('/register', register);
    app.use('/login', login);
    app.use('/user', Auth.authenticate(), user);

    return app;
}