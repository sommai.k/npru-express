import { Schema, model } from 'mongoose';

interface IUser {
    userCode: string;
    password: string;
    firstName: string;
    lastName: string;
    age: number;
}

export const UserSchema = new Schema<IUser>({
    userCode: { type: 'string', unique: true },
    password: { type: 'string', min: 8, max: 20 },
    firstName: { type: 'string', min: 1, max: 150 },
    lastName: { type: 'string', min: 1, max: 150 },
    age: { type: 'number', min: 18, max: 99 }
});

export const User = model<IUser>('User', UserSchema);