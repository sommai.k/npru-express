import { connect } from 'mongoose';
import { createServer } from './server';

const PORT = 3000;
const app = createServer();

app.listen(PORT, async () => {
    try {
        await connect(
            'mongodb://mongo:27017/nprudb',
            {
                user: 'root',
                pass: 'example',
                authSource: 'admin'
            }
        );
    } catch (e) {
        console.log(e);
    }
    console.log(`Server start on ${PORT}`)
});