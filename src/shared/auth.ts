import passport from 'passport';
import passportJwt from 'passport-jwt';
const Strategy = passportJwt.Strategy;

export const jwtKey = "yn3pbZniBT0wbWEVaHX6s7vuEDj32H4R";

class Authorize {
    constructor() {
        let strategy = new Strategy({
            secretOrKey: jwtKey,
            jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        }, (payload, done) => {
            const user = payload;
            if (user) {
                return done(null, user);
            } else {
                return done(new Error("User not found !!!"));
            }
        });
        passport.use(strategy);
    }

    initialize() {
        return passport.initialize();
    }

    authenticate() {
        return passport.authenticate('jwt', { session: false });
    }
}

export const Auth = new Authorize();
