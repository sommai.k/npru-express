import { Router, Request, Response } from 'express';
import { validationResult, query, body, param } from 'express-validator';
import { User } from '../model/user';

export const user = Router();

const deleteValidate = [
    query("userCode").notEmpty().isString().isLength({ max: 13 })
];
user.delete('', deleteValidate, async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors);
    } else {
        // business logic here
        const { userCode } = req.query;
        const user =
            await User.findOneAndDelete({ userCode }).exec();
        if (user) {
            res.json({ success: true });
        } else {
            res.json({
                success: false,
                message: 'UserCode not found'
            });
        }
    }
});

const putValidate = [
    body("firstName").notEmpty().isString().isLength({ min: 1, max: 150 }),
    body("lastName").notEmpty().isString().isLength({ min: 1, max: 150 }),
    body("age").notEmpty().isInt({ min: 18, max: 99 }),
    param("userCode").notEmpty().isString()
];
user.put('/:userCode', putValidate, async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors);
    } else {
        // Business logic here
        const { firstName, lastName, age } = req.body;
        const { userCode } = req.params;
        const user = await User.findOneAndUpdate({ userCode }, {
            firstName, lastName, age
        });
        if (user) {
            res.json({ success: true });
        } else {
            res.json({
                success: false,
                message: 'User not found !!'
            })
        }
    }
});

user.get('/', async (req: Request, res: Response) => {
    const user = await User.find({
    }, {
        password: false
    });
    res.json({
        success: true,
        data: user
    })
});