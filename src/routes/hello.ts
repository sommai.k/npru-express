import { Router, Request, Response } from 'express';
import { query, validationResult } from 'express-validator';
export const hello = Router();

const getValidate = [
    query("name").notEmpty(),
    query("age").notEmpty().isInt()
];
hello.get('/', getValidate, (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors);
    } else {
        const { name, age } = req.query;
        res.json({
            "message": `Hello ${name} (${age}) !!`
        })
    }
});

hello.post('/', (req: Request, res: Response) => {
    const { code, name } = req.body;
    res.json({
        "message": "Post data complete !!!!",
        "code": code,
        "name": name
    })
});

hello.delete('/id/:code', (req: Request, res: Response) => {
    const { code } = req.params;
    res.json({
        "message": `Delete ${code} complete !!!!`,
    })
});

hello.put("/person/:id", (req: Request, res: Response) => {
    const { room } = req.query;
    const { id } = req.params;
    const { code, name, age, active } = req.body;
    res.json({
        "message": `Update ${id} room ${room} complete !!`,
        code,
        name,
        age,
        active
    });
});