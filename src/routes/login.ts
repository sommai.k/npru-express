import { Router, Response, Request } from 'express';
import { body, validationResult } from 'express-validator';
import jwt from 'jwt-simple';
import { jwtKey } from '../shared/auth';
import { User } from '../model/user';
export const login = Router();

const postValidate = [
    body("userCode").notEmpty().isString(),
    body("password").notEmpty().isString().isLength({ min: 8, max: 50 })
];
login.post('', postValidate, async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors);
    } else {
        // business logic here
        const { userCode, password } = req.body;
        const user = await User.findOne({ userCode }).exec();
        if (user) {
            if (user.password === password) {
                // TODO: generate token here
                const { firstName, lastName, age } = user;
                const token = jwt.encode({
                    userCode
                }, jwtKey);
                res.json({
                    "success": true,
                    firstName, lastName, age, token
                });
            } else {
                res.json({
                    success: false,
                    message: 'Invalid username or password'
                })
            }
        } else {
            res.json({
                success: false,
                message: 'User not found'
            });
        }
    }
});