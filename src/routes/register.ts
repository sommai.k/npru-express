import { Router, Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { User } from '../model/user';
export const register = Router();

const postValidate = [
    body("userCode").notEmpty().isString(),
    body("firstName").notEmpty().isString().isLength({ min: 1, max: 150 }),
    body("lastName").notEmpty().isString().isLength({ min: 1, max: 150 }),
    body("password").notEmpty().isString().isLength({ min: 8, max: 20 }),
    body("age").notEmpty().isInt({ min: 18, max: 99 })
];
register.post('', postValidate, async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors);
    } else {
        const {
            userCode, firstName, lastName, password, age
        } = req.body;
        // insert to mongo
        try {
            const user = new User({
                userCode, firstName, lastName, password, age
            });
            await user.save();
            res.json({
                success: true
            });
        } catch (e) {
            res.json({
                success: false,
                message: 'Duplicate userCode'
            });
        }
    }
});