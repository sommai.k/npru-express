npm init -y
npm install typescript

## restore lib
npm install

## init typescript
npm run tsc -- --init

## compile code
npm run tsc

## run program
node dist/hello

## express and testing lib
npm i express cors body-parser
npm i passport passport-jwt
npm i --save-dev jest supertest ts-jest
npm i --save-dev @types/express @types/cors @types/body-parser
npm i --save-dev @types/jest @types/supertest
npm i --save-dev @types/passport @types/passport-jwt

npm start

# Day 2
```
npm i mongoose
npm i express-validator

mongo-express = http://localhost:8081
```

### Init Step
```
git clone https://gitlab.com/sommai.k/express-next.git
cd  express-next
docker compose up -d
```

### Clone Project
```
git clone https://gitlab.com/sommai.k/npru-express.git
cd npru-express
git remote remove origin
npm i
```

npm i jwt-simple